<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Lokum Games</title>
	<link href="images/.png" rel="icon" />
	<link href="libraries/bootstrap/bootstrap.min.css" rel="stylesheet" />
	<!-- Core Owl Carousel CSS File  *	v1.3.3 -->
	<link href="libraries/owl-carousel/owl.theme.css" rel="stylesheet" />
	<!-- Core Owl Carousel CSS Theme  File  *	v1.3.3 -->
	<link href="libraries/fonts/font-awesome.min.css" rel="stylesheet" />
	<link href="libraries/fonts/elegant/elegant-icon.css" rel="stylesheet" />
	<link href="libraries/animate/animate.min.css" rel="stylesheet" />
	<link href="libraries/lightbox2/css/lightbox.css" rel="stylesheet" />
	<link href="css/zoomtimeline.css" rel="stylesheet" />
	<link href="css/scroller.css" rel="stylesheet" />
	<link href="css/style2.css" rel="stylesheet" />

	<link href="css/components.css" rel="stylesheet" />
	<link href="css/header.css" rel="stylesheet" />
	<link href="style.css" rel="stylesheet" />
	<!--link id="color" href="css/color-schemes/default.css" rel="stylesheet"/-->
	<link href="css/media.css" rel="stylesheet" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="js/html5/html5shiv.min.js"></script>
      <script src="js/html5/respond.min.js"></script>
    <![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Merriweather:400' rel='stylesheet' type='text/css'>-->
	<link href='https://fonts.googleapis.com/css?family=Raleway:200,400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic'
					rel='stylesheet' type='text/css'>
	<script>
        zoombox_settings = {}
    </script>
</head>

<body data-offset="200" data-spy="scroll" data-target=".primary-navigation">
	<a id="top"></a>



	<!-- Slider Section -->
	<div id="photos-slider" class="slider-section">
		<div class="slides-container">
			<!-- slide item 1 -->
			<div class="slide-item">
				<img src="http://i65.tinypic.com/118kfio.png" alt="Slide 1" />
				<h2 class="slide-title">
					<span class="inner-circle">
						
					</span>
				</h2>
				
			</div>
			<!-- slide item 1 over -->

		</div>
		<nav class="slides-navigation">
			<a href="#" class="next"><i class="arrow_carrot-right"></i></a>
			<a href="#" class="prev"><i class="arrow_carrot-left"></i></a>
		</nav>
		<p class="goto-next">OYUNLAR<a class="arrow animated ci bounce" href="#why-choose"><i class="fa fa-angle-down"></i></a></p>
	</div>
	<!-- Slider Section Over-->

	<!-- Header Section -->
	<header id="header-section" class="header-section">
		<div class="col-md-2 col-sm-2 col-xs-7 logo-block">
			<a href="#top"><img src="http://i63.tinypic.com/vdq1q9.png" alt="logo" /></a>
		</div>
		<div class="col-md-10 col-sm-12 col-xs-12 menu-block">
			<nav class="navbar navbar-default primary-navigation" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
									aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav pull-right">
						<li><a href="#top">ANASAYFA</a></li>
						<li><a href="#features-section">HAKKIMIZDA</a></li>
						<li><a href="#why-choose">ZULA</a></li>
						<li><a href="#poll">POOLELITE</a></li>
						<li><a href="#social-section">SOSYAL MEDYA</a></li>
						<li><a href="#bizeulasın">BİZE ULAŞIN</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</nav>
		</div>
	</header>
	<!-- Header Section Over -->


	<!-- Features Section -->
	<section id="features-section" class="features-section lokum">
		<div class="row">

			<div class="col-md-6 col-sm-12 ">


				<div class="feature-box">
					<div class="feature-box-inner" id="kurulus">
						
						<div class="col-md-8 col-sm-7 col-lg-10">
							<h3 class="block-title">KURULUŞ 							<i class="icon-feature fa fa-building-o"></i>
</h3>

							<p>

								Lokum Games 2014 yılında, iletişim, dijital eğlence ve finans sektörlerinde deneyim sahibi kurucular tarafından milyonlarca
								oyuncuya eğlence üretmek için faaliyete geçirildi.
							</p>
						</div>

					</div>
					<div class="feature-box-inner">
						
						<div class="col-md-9 col-sm-8 col-lg-10" id="uretiyoruz">
							<h3 class="block-title">ÜRETİYORUZ 							<i class="icon-feature fa fa-trophy"></i>
</h3>
							<p>Lokum Games bahane üretmez, sınır tanımaz, hızlı çözümler üretir, daima hedef gözeterek ateş eder ve her zaman hedefi
								tam 12'den vurur. Lokum Games oyun endüstrisinin yeni öncüsü ve trendsetterıdır. Hızla büyümekte olan bir bahçe olan
								Türk oyun endüstrisinin ihtiyaç duyduğu deneyimli ve bilgili bir bahçıvan gibidir. Kardeş geliştirici şirketleriyle
								ve deneyimli iş gücüyle, piyasanın dur durak bilmez fırtınalarına karşı dayanacak esnekliğe sahiptir. En önemlisi
								de hedeflerine zamanında ulaşır.
							</p>
						</div>

					</div>
					<div class="feature-box-inner">
						
						<div class="col-md-10 col-sm-9 col-lg-10" id="takip">
							<h3 class="block-title">TAKİPTEYİZ 							<i class="icon-feature fa fa-tachometer"></i>
</h3>
							<p>Lokum Games oyun sektörünün lokalizasyon, yayıncılık ve adaptasyon zorluklarının farklında olan ve bu zorlukları aşarken
								en son teknolojileri ve yeni yaklaşımları harmanlama konusunda tereddüt etmeyen, inovatif çözümler üretebilen, genç
								ve dinamik profesyonellerden oluşmaktadır. Lokum Games ekibi oyun yapım süreçlerini de çok iyi bilir.
							</p>
						</div>

					</div>
					<div class="feature-box-inner" >
						
						<div class="col-md-11 col-sm-10 col-lg-10" id="buyuyoruz">
							<h3 class="block-title">BÜYÜYORUZ							<i class="icon-feature fa fa-expand"></i>
</h3>
							<p>

								Lokum Games'in reaktörleri şirket bünyesinde geliştirdiği yenilikçi programlarla topladığı nokta atışı verileri kesin bir
								şekilde analiz ederek, doğru strateji üretmek için 7-24 çalışır. Bu yenilikçi programlar, data toplama, ürün yönetimi
								ve kalite kontrol aşamalarında geliştiricilere büyük katkılar sağlayarak, ürünlerin ihtiyaç duyduğu bir adım önde
								olma halini sağlar.</p>
						</div>

					</div>


				</div>
			</div>
			<div class="col-md-5">
				<div style="text-align: left">
					<div class="zoomtimeline mode-3dslider auto-init zoomtimeline0 skin-light circuit-the-timeline-on inited ztm-ready" data-options="{startItem: 1}"
									id="zoomtimeline5">











						<div class="details-container" style="height: 651px;">
							<div class="clear"></div>


							<input type="radio" name="radio_btn" id="it1" checked="">
							<label for="it1" class="detail ">


                                <div class="the-year">2014<figure></figure></div>
                                <h3 class="the-heading">Lokum Games kuruldu. </h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_1.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                <p>
								
                                </p>
                            </div><div class="clear"></div>


                            </label>



							<input type="radio" name="radio_btn" id="it2" checked="">
							<label for="it2" class="detail ">


                                <div class="the-year">2014<figure></figure></div>
                                <br>  <br>
                                <h3 class="the-heading">Şubat Zula Açık Betaya başladı</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_2.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                <p>
Haziran Pool Elite Kapalı Beta Başladı. Dünya şampiyonu Semih Saygıner'in katkılarıyla geliştirilen Pool Elite ilk kez oyuncularıyla buluştu.                                </p>
                            </div><div class="clear"></div>


                            </label>


							<input type="radio" name="radio_btn" id="it3">
							<label for="it3" class="detail ">


                                <div class="the-year">2014<figure></figure></div>
                                  <br>  <br>
                                <h3 class="the-heading">Ekim Zula Kapalı Beta Başladı</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_3.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div>

                                <div class=" detail-excerpt">


                                    <p>
Ekim Zula Kapalı Beta Başladı. Türkiye'nin ilk ve tek MMOFPS oyunu Zula ilk kez oyuncularıyla buluştu. Kapalı beta rekor başvuru aldı.                                    </p>
                                </div>
                                <div class="clear"></div>
                            </label>


							<input type="radio" name="radio_btn" id="it4">
							<label for="it4" class="detail ">

                                <div class="the-year">2015<figure></figure></div>

                                <h3 class="the-heading">250.000 aylık aktif kullanıcı</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_4.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                <p>
Ocak Pool Elite Facebook bilardo oyunları arasında 250.000 aylık aktif kullanıcı ile 4. Sıraya yerleşti                                </p>
                            </div><div class="clear"></div>

                            </label>


							<input type="radio" name="radio_btn" id="it5">
							<label for="it5" class="detail next-next-item">

                                <div class="the-year">2015<figure></figure></div>

                                <h3 class="the-heading">Şubat Zula Açık Betaya başladı</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_5.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                <p>
- Şubat Zula Açık Betaya başladı<br> Kasım Pool Elite Kristal Piksel Video Oyun Ödüllerinde " En İyi Görsel" ödülüne layık görüldü.<br>
Aralık Zula Gamex 2015'te "En İyi Yerli Oyun" ödülüne layık görüldü. 

                       </p>
                            </div><div class="clear"></div>

                            </label>





							<input type="radio" name="radio_btn" id="it6">
							<label for="it6" class="detail ">

                                <div class="the-year">2016<figure></figure></div>

                                <h3 class="the-heading"> </h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_6.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                               
                            </div><div class="clear"></div>

                            </label>


	<input type="radio" name="radio_btn" id="it7">
							<label for="it7" class="detail ">

                                <div class="the-year">2016<figure></figure></div>

                                <h3 class="the-heading"></h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_7.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                
                            </div><div class="clear"></div>

                            </label>


	<input type="radio" name="radio_btn" id="it8">
							<label for="it8" class="detail ">

                                <div class="the-year">2016<figure></figure></div>

                                <h3 class="the-heading"> </h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_8.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                               
                            </div><div class="clear"></div>

                            </label>


	<input type="radio" name="radio_btn" id="it9">
							<label for="it9" class="detail ">

                                <div class="the-year">2016<figure></figure></div>

                                <h3 class="the-heading"> </h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_9.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                
                            </div><div class="clear"></div>

                            </label>



                            	<input type="radio" name="radio_btn" id="it10">
							<label for="it10" class="detail ">

                                <div class="the-year">2016<figure></figure></div>

                                <h3 class="the-heading"></h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(img/3d_10.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                               
                                <!--<p>-->
                                <!--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.-->
                                <!--</p>-->
                            </div><div class="clear"></div>

                            </label>

						</div>
					</div>
				</div>

			</div>

		</div>
	</section>
	<!-- Features Section Over -->



	<!-- Why Choose Section -->
	<section id="why-choose" class="why-choose">
		<div class="container">
			<div class="col-md-7 col-lg-7 col-sm-8 pull-right ">
				<div class="why-choose-inner">
					<h2>ZULA</h2>
					<div class="why-choose-box ">
						<p>Zula, ilk Türk MMOFPS oyunu olmanın haklı gururunu taşıyor. Düşük sistem gereksinimleri istemesine rağmen harika grafikleri
							olan Zula'da oyuncular, tanıdık simalara sahip karakterlerle, Üsküdar sahil ya da metro istasyonu gibi bilindik yerlerin
							haritalarında kıyasıya mücadele edebiliyorlar.
						</p>

						<p>Sürekli yenilenen içerikleri ile gelişmede dur durak tanımayan Zula, espor alanında da yaptığı organizasyonlarla adından
							söz ettiriyor. Her ay düzenli olarak yapılan nakit para ödüllü turnuvalarla espor alanında faaliyetlerine tam gaz
							devam ediyor.</p>

						<div class="statistics-section ">

							<div class="col-sm-6">
								<h1><i class="fa fa-user " style="color:#d21111"></i> <span id="work"></span> </h1>
								<p>OYUNCU</p>
							</div>

							<div class="col-sm-6 macsayisi" >
								<h1><i class="fa fa-money" style="color:#d21111"></i> <span id="consumed"></span> TL </h1>
								<p> ÖDÜL DAĞITILDI</p>
							</div>


							<div class="col-sm-12" style="padding-top: 20px">

								<h1><i ><img src="http://i68.tinypic.com/34xjlzn.png" alt=""></i> <span id="videos"></span> </h1>
								<p>LOKUM YEDİK</p>


						</div>

					</div>

	<div class="col-sm-12 col-md-12 col-lg-12 text-center"> 
		
<button class="btn btn-danger btn-lg"><a href="http://www.zulaoyun.com" style="color:white">WWW.ZULAOYUN.COM</a></button>
	</div>
				</div>
	</section>
	<!-- Why Choose Section Over -->


	<section id="poll" class="features-section poll">
		<div class="container">
			<div class="col-md-6    col-sm-11 pull-left animated fadeInRight">
				<div class="why-choose-inner">
					<h2>POOL ELİTE</h2>
					<div class="why-choose-box polltablet ">
						<p>
							8 Top Amerikan, Karambol, Snooker, Üç Bant Bilardo ve 9 Top oyun modları ile Pool Elite, cross platform yapısıyla tarayıcıların
							yanı sıra Android, IOS yazılımlı mobil cihazlarda oynanabilen ilk ve tek 3 boyutlu bilardo oyunu olma özelliğine sahip.
						</p>
						<p>Unity ile geliştirilen ve en gerçekçi oyun fiziğinin yanı sıra yine üç boyutlu odaları, özel tasarım istekaları ve
							bilardo ekipmanları ile Pool Elite aynı zamanda profesyonel bilardo oyuncularına da hitap ediyor.</p>
						<p>Dünyanın her yerinden kullanıcıların bulunduğu haftalık ve sezonluk lig sisteminde mücadele oyuncular, dilerlerse Pool
							Elite arkadaşlarını dostluk maçına davet edebiliyor.</p>
						<p>Milli bilardocumuz Semih Saygıner'in bizzat geliştirme sürecine katkıda bulunduğu Pool Elite, şu anda tarayıcılardan
							Facebook’ta oynanabiliyor ve çok yakında Android ile IOS platformlarında mobil uygulamasıyla da yer alacak.</p>

						<div class="statistics-section">

							<div class="col-sm-6">
								<h1><i class="fa fa-user " style="color:#5c9a2f"></i> <span id="pool"></span> </h1>
								<p> YAPILAN MAÇ SAYISI</p>
							</div>

							<div class="col-sm-6 macsayisi">

								<h1><i class="fa fa-money" style="color:#5c9a2f"></i> <span id="pooliki"></span> </h1>
								<p>ÜLKEDE OYNANIYOR</p>
							</div>




							<div class="col-sm-8 col-lg-5"  style="padding: 25px">


								<h1><i style="color:#5c9a2f"><img src="http://i65.tinypic.com/f4k8qh.png"  width="35px" alt=""></i> <span id="pooluc"></span> </h1>
								<p> YIRTILAN ÇUHALAR</p>
							

							</div>

						
							<div class="col-sm-4 col-lg-7"   >
								<a href="https://apps.facebook.com/poolelite/?fb_source=search&ref=ts&fref=ts" target="_blank"><p style="    font-size: 20px;"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i> FACEBOOK'TA OYNA</p></a>
								
							</div>








						</div>
					</div>



				</div>
			</div>
		</div>
	</section>















	
	<!-- Social Section -->
	<div id="social-section" class="social-main">
		<div class=" icon-social-google no-padding">
			<a href="https://play.google.com/store/apps/dev?id=4895078683675015711" target="_blank"><i> <img src="http://img00.deviantart.net/9991/i/2012/066/a/c/google_play_icon___logo_by_chrisbanks2-d4s1i75.png" width="55px;" alt=""></i></a>
		</div>
		
		<div class="icon-social-facebook no-padding">
			<a href="https://www.facebook.com/LokumGamesInc/" target="_blank"><i class=" social_facebook"></i></a>
		</div>
		<div class="icon-social-vimoe no-padding">
			<a href="https://www.linkedin.com/company/lokum-games" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
		</div>
		<div class="icon-social-plus no-padding">
			<a href="https://plus.google.com/u/0/102606664071188743212" target="_blank"><i ><i class="fa fa-google-plus-square" aria-hidden="true"></i>
</i></a>
		</div>
		<div class="icon-social-twitter no-padding">
			<a href="https://twitter.com/LokumGamesInc" target="_blank"><i class="social_twitter "></i></a>
		</div>
		<div class="icon-social-rss no-padding">
			<a href="https://www.youtube.com/channel/UCBVb8fgqk7nDdZA3ay8-JRA" target="_blank"><i ><i class="fa fa-youtube" aria-hidden="true"></i></i></a>
		</div>



	</div>

	<!-- Social Section Over -->

	<!-- Map Section -->

	<!-- Map Section Over -->

	<!-- Project Section -->
	<div id="bizeulasın" class="project-section">
		<div class="container">
			<div class="col-md-6 hidden-sm hidden-xs start-project">
				
				<h2>OYUNLARINIZI</h2>
				<h2>YAYINLAYALIM</h2>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 send-msg">
				
				<h2 style="color:white">BİZE ULAŞIN</h2>
				<a href="#" data-toggle="modal" data-target="#myModal"><span>Mesaj Gönder</span></a>
			</div>
		</div>
	</div>
	<!-- Project Section Over -->

	<!-- Footer Section -->
	<footer id="footer-section" class="footer-section">
		<div class="footer-item">
			<i class="icon_mail_alt"></i>
			<div class="footer-inner">
				<p class="footer-item-title">E-mail</p>
				<a class="footer-item-desc">contact@lokumgames.com</a>
			</div>
		</div>
		<div class="footer-item">
			<i class="fa fa-map-marker" aria-hidden="true"></i>
			<div class="footer-inner">
				<p class="footer-item-title">Adres</p>
				<p class="footer-item-desc">Lokum Games 
Şişli/İstanbul </p>
			</div>
		</div>
		<a id="back-to-top" class="back-top pull-right"><i class="arrow_up"></i>Yukarı </a>
	</footer>
	<!-- Footer Section -->

	<!-- Light Box -->
	<div class="modal light-box fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="false">
		<!-- Modal -->
		<div class="container">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close icon_close" data-dismiss="modal" aria-label="Close"></button>
							
						</div>
						<div id="contact" class="modal-body">
							<div class="popup-form">
								<form action="./contact.php" target="hidden" method="post">
									<div id="alert-msg" class="alert-msg"></div>
									<div class="form-group col-md-6 col-sm-6">
										<input type="text" class="form-control" id="input_name" name="contact-name" placeholder="İSİM SOYİSİM*" required="required" />
									</div>
									<div class="form-group col-md-6 col-sm-6">
										<input type="text" class="form-control" id="input_compnay_name" name="contact-company-name" placeholder="ŞİRKET ADINIZ" />
									</div>
									<div class="form-group col-md-6 col-sm-6">
										<input type="email" class="form-control" id="input_email" name="contact-email" placeholder="EMAİL*" required="required" />
									</div>
									<div class="form-group col-md-6 col-sm-6">
										<input type="text" class="form-control" id="input_phone" name="contact-phone" placeholder="TELEFON" />
									</div>
									
									<input type="submit" value="Gönder" class="send" />
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Light Box -->


	<!-- jQuery Include -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

	<script src="libraries/modernizr/modernizr.custom.13711.js"></script>
	<script src="libraries/jquery.easing.min.js"></script>
	<!-- Easing Animation Effect -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/superslides/0.6.2/jquery.superslides.min.js" integrity="sha256-BJ5ztlXSWc6WOZ5VMVCJgtKomDBVPstatOGAeCmDIZ8=" crossorigin="anonymous"></script>
	<script src="libraries/bootstrap/bootstrap.min.js"></script>
	<!-- Core Bootstrap v3.2.0 -->
	<script src="libraries/bootstrap/ie-emulation-modes-warning.js"></script>
	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<script src="libraries/bootstrap/ie10-viewport-bug-workaround.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- Font Elegant -->
	<!--[if lte IE 7]><script src="libraries/fonts/elegant/lte-ie7.js"></script><![endif]-->

	<script src="libraries/portfolio-filter/jquery.quicksand.js"></script>
	<!-- Quicksand v1.4 -->
	<!-- Superslides - v0.6.3-wip -->

	<script src="libraries/roundabout.js"></script>
	<!-- service Rounded slider -->
	<script src="libraries/roundabout_shapes.js"></script>
	<!-- service Rounded slider -->

	<script src="libraries/jquery.animateNumber.min.js"></script>
	<!-- Used for Animated Numbers -->
	<script src="libraries/jquery.appear.js"></script>
	<!-- It Loads jQuery when element is appears -->
	<script src="libraries/jquery.knob.js"></script>
	<!-- Used for Loading Circle -->

	<script src="libraries/wow.min.js"></script>
	<script src="libraries/lightbox2\js\lightbox.min.js"></script>

	<!-- Core Owl Carousel CSS File  *	v1.3.3 -->


	<!-- Customized Scripts -->
	<script src="js/functions.js"></script>


	


</body>



</html>